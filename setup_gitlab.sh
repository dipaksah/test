#!/bin/bash
# run this script as root via the syntax below
# curl -s url_to_this_script | bash /dev/stdin the_password the_backup

if [ "$1" != "" ]; then
  export MY_PWD="$1"
else
  export MY_PWD="changeme"
fi

if [ "$2" != "" ]; then
  export MY_BAK="$2"
fi

export MY_USER="gitlab"
useradd $MY_USER
echo "$MY_PWD" | passwd $MY_USER --stdin
usermod -aG wheel $MY_USER
su -m $MY_USER
cd /home/$MY_USER

echo "$MY_PWD" | sudo -S yum install -y wget expect unzip
curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash
echo "$MY_PWD" | sudo -S yum install -y gitlab-ce-9.5.2
echo "$MY_PWD" | sudo -S gitlab-ctl reconfigure

if [ "$MY_BAK" != "" ]; then
  cd /home/$MY_USER
  wget $MY_BAK -O backup.zip
  unzip backup.zip
  cd cicd*
  cd gitlab
  echo "$MY_PWD" | sudo -S mv *backup.tar /var/opt/gitlab/backups/
  sudo gitlab-ctl stop unicorn
  sudo gitlab-ctl stop sidekiq
  
  /usr/bin/expect <<END
  spawn sudo gitlab-rake gitlab:backup:restore
  while {1} {
    expect {
      "gitlab:" { send "$MY_PWD\r" }
      "yes/no)?" { send "yes\r" }
      -re . { exp_continue }
      eof { exit }
    }
  }
  # wait
  # close $spawn_id
END
  
  sudo gitlab-ctl start
  sudo gitlab-rake gitlab:check SANITIZE=true
fi
sudo reboot
